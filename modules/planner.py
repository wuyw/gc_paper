from typing import Dict, List
from langchain.chat_models import ChatOpenAI
from langchain.schema import HumanMessage, SystemMessage

class OutlinePlanner:
    def __init__(self):
        self.chat_model = ChatOpenAI(
            model_name="gpt-3.5-turbo",
            temperature=0.7
        )
    
    def analyze_topic(self, topic: str, requirements: str) -> Dict:
        """分析论文主题"""
        prompt = f"""
        请分析以下学术论文主题和要求:
        
        主题: {topic}
        要求: {requirements}
        
        请提供以下分析:
        1. 研究问题的关键点
        2. 可能的研究方法
        3. 需要查找的相关文献领域
        4. 潜在的创新点
        5. 可能遇到的挑战
        
        以JSON格式返回分析结果。
        """
        
        try:
            messages = [
                SystemMessage(content="你是一个专业的学术论文主题分析专家。"),
                HumanMessage(content=prompt)
            ]
            response = self.chat_model.generate([messages])
            return eval(response.generations[0][0].text)
        except Exception as e:
            print(f"主题分析出错: {str(e)}")
            return {
                "key_points": [],
                "methods": [],
                "literature_areas": [],
                "innovation": [],
                "challenges": []
            }
    
    def generate_outline(self, analysis: Dict) -> Dict:
        """生成论文大纲"""
        prompt = f"""
        基于以下分析生成详细的论文大纲:
        {str(analysis)}
        
        请包含以下部分:
        1. 摘要结构
        2. 引言框架
        3. 文献综述要点
        4. 研究方法步骤
        5. 预期结果讨论
        6. 结论要素
        
        以JSON格式返回大纲。
        """
        
        try:
            messages = [
                SystemMessage(content="你是一个专业的学术论文大纲规划专家。"),
                HumanMessage(content=prompt)
            ]
            response = self.chat_model.generate([messages])
            return eval(response.generations[0][0].text)
        except Exception as e:
            print(f"大纲生成出错: {str(e)}")
            return {
                "abstract": [],
                "introduction": [],
                "literature_review": [],
                "methodology": [],
                "results_discussion": [],
                "conclusion": []
            }
    
    def update_outline(self, outline: Dict, feedback: str) -> Dict:
        """根据反馈更新大纲"""
        prompt = f"""
        请根据以下反馈修改论文大纲:
        
        当前大纲:
        {str(outline)}
        
        反馈意见:
        {feedback}
        
        请提供修改后的大纲(JSON格式)。
        """
        
        try:
            messages = [
                SystemMessage(content="你是一个专业的学术论文大纲修改专家。"),
                HumanMessage(content=prompt)
            ]
            response = self.chat_model.generate([messages])
            return eval(response.generations[0][0].text)
        except Exception as e:
            print(f"大纲更新出错: {str(e)}")
            return outline 