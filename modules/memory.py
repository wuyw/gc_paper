class ContextManager:
    def __init__(self):
        self.memory = {}
        self.cache = {}
    
    def add_context(self, key: str, value: any):
        """添加上下文信息"""
        self.memory[key] = value
    
    def get_context(self, key: str) -> any:
        """获取上下文信息"""
        return self.memory.get(key)
    
    def cache_result(self, key: str, value: any):
        """缓存结果"""
        self.cache[key] = value
    
    def get_cached_result(self, key: str) -> any:
        """获取缓存的结果"""
        return self.cache.get(key) 