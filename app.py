import gradio as gr
from modules.agent import PaperWritingAgent
from modules.tools import (
    LiteratureSearchTool,
    TextGenerationTool,
    GrammarCheckTool,
    FormattingTool
)
from modules.planner import OutlinePlanner
from modules.memory import ContextManager

class PaperWritingApp:
    def __init__(self):
        self.agent = PaperWritingAgent()
        self.context_manager = ContextManager()
        
    def analyze_topic(self, topic, requirements):
        """分析论文主题和要求"""
        return self.agent.analyze_topic(topic, requirements)
    
    def generate_outline(self, topic_analysis):
        """生成论文大纲"""
        return self.agent.generate_outline(topic_analysis)
    
    def write_section(self, section_name, outline):
        """生成特定章节的内容"""
        return self.agent.write_section(section_name, outline)
    
    def review_and_polish(self, content):
        """审查和润色内容"""
        return self.agent.review_and_polish(content)
    
    def generate_full_paper(self, outline: str, max_words: int) -> str:
        """生成完整论文"""
        return self.agent.generate_full_paper(outline, max_words)
    
    def generate_paper_pipeline(self, topic: str, requirements: str, max_words: int) -> tuple[str, str]:
        """一键生成论文的流水线处理"""
        # 1. 分析主题
        analysis = self.agent.analyze_topic(topic, requirements)
        
        # 2. 生成大纲
        outline = self.agent.generate_outline(analysis)
        
        # 3. 生成论文
        paper = self.agent.generate_full_paper(outline, max_words)
        
        return outline, paper

def create_ui():
    app = PaperWritingApp()
    
    with gr.Blocks() as interface:
        gr.Markdown("# 学术论文写作助手")
        
        with gr.Tab("论文生成"):
            # 第一步：输入主题和要求
            topic_input = gr.Textbox(
                label="论文主题",
                placeholder="请输入论文主题，例如：人工智能在教育领域的应用"
            )
            requirements_input = gr.Textbox(
                label="具体要求",
                placeholder="请输入具体要求，例如：研究方向、字数限制等",
                lines=3
            )
            max_words = gr.Slider(
                minimum=5000,
                maximum=20000,
                value=10000,
                step=1000,
                label="论文字数"
            )
            
            # 生成按钮
            generate_btn = gr.Button("一键生成论文")
            
            # 输出区域
            with gr.Accordion("生成结果", open=False):
                outline_output = gr.Textbox(label="论文大纲", lines=5)
                paper_output = gr.Textbox(label="论文内容", lines=30)
            
            # 处理流程
            generate_btn.click(
                fn=app.generate_paper_pipeline,
                inputs=[topic_input, requirements_input, max_words],
                outputs=[outline_output, paper_output]
            )
        
        with gr.Tab("润色完善"):
            content_input = gr.Textbox(
                label="待润色内容",
                lines=10,
                placeholder="请输入需要润色的内容..."
            )
            polish_btn = gr.Button("润色内容")
            polished_output = gr.Textbox(label="润色后的内容", lines=10)
            
            polish_btn.click(
                fn=app.review_and_polish,
                inputs=content_input,
                outputs=polished_output
            )
    
    return interface

if __name__ == "__main__":
    interface = create_ui()
    interface.launch() 