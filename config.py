import os
from dotenv import load_dotenv

# 加载 .env 文件中的环境变量
load_dotenv()

class Config:
    # OpenAI 配置
    OPENAI_API_KEY = os.getenv("OPENAI_API_KEY")
    OPENAI_API_BASE = os.getenv("OPENAI_API_BASE", "https://api.openai.com/v1")
    OPENAI_MODEL = os.getenv("OPENAI_MODEL", "gpt-3.5-turbo")
    OPENAI_TEMPERATURE = float(os.getenv("OPENAI_TEMPERATURE", "0.7"))
    
    # Semantic Scholar API 配置
    SEMANTIC_SCHOLAR_API_BASE = os.getenv(
        "SEMANTIC_SCHOLAR_API_BASE", 
        "https://api.semanticscholar.org/v1"
    )
    
    # LanguageTool API 配置
    LANGUAGE_TOOL_API_URL = os.getenv(
        "LANGUAGE_TOOL_API_URL", 
        "https://api.languagetool.org/v2/check"
    )
    
    # 论文格式配置
    DEFAULT_PAPER_STYLE = os.getenv("DEFAULT_PAPER_STYLE", "APA")
    SUPPORTED_PAPER_STYLES = ["APA", "MLA", "Chicago", "Harvard"] 